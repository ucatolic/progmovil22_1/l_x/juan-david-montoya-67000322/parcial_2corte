package com.example.parcial2

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class DataClient: AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.data_client)
        showInformation()
    }

    fun showInformation () {
        val bundle = intent.extras
        val name = bundle?.get("INTENT_NAME")
        val txtNameClient:TextView = findViewById(R.id.txtName)
            txtNameClient.text = "¡Hola $name!"
        val phone = bundle?.get("INTENT_PHONE")
        val phoneClient:TextView = findViewById(R.id.txtPhone)
            phoneClient.text = "N° $phone"
        val type = bundle?.get("INTENT_TYPE")
        val ty:TextView = findViewById(R.id.txtType)
            ty.text = "$type"
        val location = bundle?.get("INTENT_LOCATION")
        val lugar:TextView = findViewById(R.id.txtLocation2)
            lugar.text = "$location"
        val correo = bundle?.get("INTENT_EMAIL")
        val email:TextView = findViewById(R.id.txtEmail)
            email.text = "$correo"
    }
}