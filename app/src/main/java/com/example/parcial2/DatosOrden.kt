package com.example.parcial2

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class DatosOrden : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.orden_final)
        showInfo()
        val btnHi:Button = findViewById(R.id.devolver)
            btnHi.setOnClickListener { salir() }
    }

    fun showInfo() {
        val bundle = intent.extras
        val name = bundle?.get("INTENT_NAME")
        val txtNameClient:TextView = findViewById(R.id.txtName1)
            txtNameClient.text = "¡Hola $name!"
        val Proteina = bundle?.get("INTENT_Proteina")
        val tvResult6: TextView = findViewById(R.id.txtProteina)
            tvResult6.text = "Proteinas: $Proteina "
        val Pan = bundle?.get("INTENT_Pan")
        val tvResult7: TextView = findViewById(R.id.txtPan)
            tvResult7.text = "Pan: $Pan "
        val Vegetales = bundle?.get("INTENT_Vegetales")
        val tvResult8: TextView = findViewById(R.id.txtVegetales)
            tvResult8.text = "Vegetales: $Vegetales "
        val Salsas = bundle?.get("INTENT_Salsa")
        val tvResult9: TextView = findViewById(R.id.txtSalsas)
            tvResult9.text = "Salsas: $Salsas "
        val Combos = bundle?.get("INTENT_Combos")
        val tvResult10: TextView = findViewById(R.id.txtComboFinal)
            tvResult10.text = "Combo de Papas y Gaseosa: $Combos "
    }

    fun salir() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}