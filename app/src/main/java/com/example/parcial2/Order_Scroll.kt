package com.example.parcial2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*

class Order_Scroll : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_scroll)

        val spn2: Spinner =findViewById(R.id.spinnerProtein)
        val pro = resources.getStringArray(R.array.optionsProteina)
        val adaptador2 = ArrayAdapter(this, android.R.layout.simple_spinner_item, pro)
        spn2.adapter = adaptador2

        val btnHi: Button = findViewById(R.id.ordenar)
        btnHi.setOnClickListener { checkData() }
    }

    fun checkData() {
        var validacion:Boolean = false
        var texto:String = ""
        var textobox:String = ""
        var textosalsa:String = ""
        var textocombo:String = ""

        //Proteína
        val etProte: Spinner = findViewById(R.id.spinnerProtein)
            if (etProte.selectedItem.equals("")) {
                validacion = true
                msg("Faltan datos por completar en la Proteina")
            }

        // Opciones de Pan
        val rb1 = findViewById<View>(R.id.breadWhite) as RadioButton
        val rb2 = findViewById<View>(R.id.breadOregano) as RadioButton
        val rb3 = findViewById<View>(R.id.breadIntegral) as RadioButton
        val rb4 = findViewById<View>(R.id.breadAvena) as RadioButton
            if (!rb1.isChecked && !rb2.isChecked && !rb3.isChecked && !rb4.isChecked) {
                validacion = true
                msg("Faltan datos por completar en las opciones de Pan")
            } else {
                when {
                    rb1.isChecked -> { texto = rb1.getText().toString() }
                    rb2.isChecked -> { texto = rb2.getText().toString() }
                    rb3.isChecked -> { texto = rb3.getText().toString() }
                    rb4.isChecked -> { texto = rb4.getText().toString() }
                }
            }

        // Opciones de Vegetales
        val chbox1 = findViewById<View>(R.id.tomate) as CheckBox
        val chbox2 = findViewById<View>(R.id.cebolla) as CheckBox
        val chbox3 = findViewById<View>(R.id.lechuga) as CheckBox
        val chbox4 = findViewById<View>(R.id.pepinillos) as CheckBox
        val chbox5 = findViewById<View>(R.id.pepino) as CheckBox
        val chbox6 = findViewById<View>(R.id.noVegetales) as CheckBox
            if (!chbox1.isChecked && !chbox2.isChecked && !chbox3.isChecked && !chbox4.isChecked && !chbox5.isChecked && !chbox6.isChecked) {
                validacion = true
                msg("Faltan datos por completar en las opciones de vegetales")
            } else {
                if (chbox1.isChecked ) { textosalsa += chbox1.getText().toString() + " " }
                if (chbox2.isChecked ) { textosalsa += chbox2.getText().toString() + " " }
                if (chbox3.isChecked ) { textosalsa += chbox3.getText().toString() + " " }
                if (chbox4.isChecked ) { textosalsa += chbox4.getText().toString() + " " }
                if (chbox5.isChecked ) { textosalsa += chbox5.getText().toString() + " " }
                if (chbox6.isChecked ) { textosalsa += chbox6.getText().toString() + " " }
            }

        // Opciones de Salsas
        val chbox7 = findViewById<View>(R.id.bbq) as CheckBox
        val chbox8 = findViewById<View>(R.id.chiplote) as CheckBox
        val chbox9 = findViewById<View>(R.id.tomateSalsa) as CheckBox
        val chbox10 = findViewById<View>(R.id.mayonesa) as CheckBox
        val chbox11 = findViewById<View>(R.id.rosada) as CheckBox
        val chbox12 = findViewById<View>(R.id.mostaza) as CheckBox
        val chbox13 = findViewById<View>(R.id.ajo) as CheckBox
        val chbox14 = findViewById<View>(R.id.ninguna) as CheckBox
            if (!chbox7.isChecked && !chbox8.isChecked && !chbox9.isChecked && !chbox10.isChecked && !chbox11.isChecked && !chbox12.isChecked && !chbox13.isChecked && !chbox14.isChecked) {
                validacion = true
                msg("Faltan datos por completar en las opciones de salsas")
            } else {
                if (chbox7.isChecked ) { textobox += chbox7.getText().toString() + " " }
                if (chbox8.isChecked ) { textobox += chbox8.getText().toString() + " " }
                if (chbox9.isChecked ) { textobox += chbox9.getText().toString() + " " }
                if (chbox10.isChecked ) { textobox += chbox10.getText().toString() + " " }
                if (chbox11.isChecked ) { textobox += chbox11.getText().toString() + " " }
                if (chbox12.isChecked ) { textobox += chbox12.getText().toString() + " " }
                if (chbox13.isChecked ) { textobox += chbox13.getText().toString() + " " }
                if (chbox14.isChecked ) { textobox += chbox14.getText().toString() + " " }
            }

        // Opciones de Combos
        val rb5 = findViewById<View>(R.id.selecSI) as RadioButton
        val rb6 = findViewById<View>(R.id.selecNO) as RadioButton
            if (!rb5.isChecked && !rb6.isChecked ) {
                validacion = true
                msg("La orden no fue completada, datos incompletos en su Combo (Gasesosa y papas)")
            } else {
                when {
                    rb5.isChecked -> { textocombo = rb5.getText().toString() }
                    rb6.isChecked -> { textocombo = rb6.getText().toString() }
                }
            }


        if (!validacion) {
            val bundle=intent.extras
            val name = bundle?.get("INTENT_NAME")
            val intent = Intent(this, DatosOrden::class.java)
                intent.putExtra("INTENT_NAME", name.toString())
                intent.putExtra("INTENT_Proteina", etProte.selectedItem.toString())
                intent.putExtra("INTENT_Pan", texto)
                intent.putExtra("INTENT_Vegetales", textosalsa)
                intent.putExtra("INTENT_Salsa", textobox)
                intent.putExtra("INTENT_Combos", textocombo)
                startActivity(intent)
        }
    }

    fun msg(msg:String) { Toast.makeText(this,msg, Toast.LENGTH_SHORT).show() }
}