package com.example.parcial2

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class AboutMe:AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about)
        showInfo()
    }

    fun showInfo () {
        val bundle = intent.extras
        val name = bundle?.get("INTENT_NAME")
        val txtNameDevolper:TextView = findViewById(R.id.txtName)
            txtNameDevolper.text = "$name"
        val code = bundle?.get("INTENT_CODE")
        val codeDeveloper:TextView = findViewById(R.id.txtCode)
            codeDeveloper.text = "Código: $code"
        val employed = bundle?.get("INTENT_EMPLOYED")
        val emplo:TextView = findViewById(R.id.txtEmployed)
            emplo.text = "$employed"
        val location = bundle?.get("INTENT_LOCATION")
        val lugar:TextView = findViewById(R.id.txtLocation)
            lugar.text = "$location"
    }
}