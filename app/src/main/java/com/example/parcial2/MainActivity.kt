package com.example.parcial2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbarHamburguer))
        val btnCargaDatos:Button = findViewById(R.id.save)
            btnCargaDatos.setOnClickListener { checkDatos() }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.orden -> {
            val name: EditText = findViewById(R.id.editTextTextPersonName)
            val phone: EditText = findViewById(R.id.editTextPhone)
            val type: Spinner = findViewById(R.id.spinner2)
            val location: EditText = findViewById(R.id.editTextTextPostalAddress)
            val email: EditText = findViewById(R.id.editTextTextEmailAddress)
            if (name.text.isNotEmpty() && phone.text.isNotEmpty() && location.text.isNotEmpty() && email.text.isNotEmpty()) {
                val intent = Intent(this, Order_Scroll::class.java)
                    intent.putExtra("INTENT_NAME", name.text)
                    intent.putExtra("INTENT_PHONE", phone.text)
                    intent.putExtra("INTENT_TYPE", type.selectedItem.toString())
                    intent.putExtra("INTENT_LOCATION", location.text)
                    intent.putExtra("INTENT_EMAIL", email.text)
                    startActivity(intent)
            } else { msg("No puedes continuar sin brindarnos tus datos.") }
            true
        }

        R.id.data -> {
            val name: EditText = findViewById(R.id.editTextTextPersonName)
            val phone: EditText = findViewById(R.id.editTextPhone)
            val type: Spinner = findViewById(R.id.spinner2)
            val location: EditText = findViewById(R.id.editTextTextPostalAddress)
            val email: EditText = findViewById(R.id.editTextTextEmailAddress)
            if (name.text.isNotEmpty() && phone.text.isNotEmpty() && location.text.isNotEmpty() && email.text.isNotEmpty()) {
                val intent = Intent(this, DataClient::class.java)
                    intent.putExtra("INTENT_NAME", name.text)
                    intent.putExtra("INTENT_PHONE", phone.text)
                    intent.putExtra("INTENT_TYPE", type.selectedItem.toString())
                    intent.putExtra("INTENT_LOCATION", location.text)
                    intent.putExtra("INTENT_EMAIL", email.text)
                    startActivity(intent)
            } else { msg("No podemos mostrar la información del cliente, no tenenos todos los datos por favor revisa.") }
            true
        }

        R.id.data_developer -> {
            val intent = Intent(this, AboutMe::class.java)
                intent.putExtra("INTENT_NAME", "Juan David Montoya Lesmes")
                intent.putExtra("INTENT_CODE", "67000322")
                intent.putExtra("INTENT_EMPLOYED", "Quality Assurance Enginner")
                intent.putExtra("INTENT_LOCATION", "Universidad Católica de Colombia")
                startActivity(intent)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    fun checkDatos() {
        val nombres:EditText = findViewById(R.id.editTextTextPersonName)
        val phone:EditText = findViewById(R.id.editTextPhone)
        val type:Spinner = findViewById(R.id.spinner2)
        val location:EditText = findViewById(R.id.editTextTextPostalAddress)
        val email:EditText = findViewById(R.id.editTextTextEmailAddress)
        if (nombres.text.isNotEmpty() && phone.text.isNotEmpty() && location.text.isNotEmpty() && email.text.isNotEmpty()) {
            Toast.makeText(this, "Sus datos fueron almacenados, Bienvenido!", Toast.LENGTH_LONG).show()
            val intent = Intent(this, DataClient::class.java)
                intent.putExtra("INTENT_NAME", nombres.text)
                intent.putExtra("INTENT_PHONE", phone.text)
                intent.putExtra("INTENT_TYPE", type.selectedItem.toString())
                intent.putExtra("INTENT_LOCATION", location.text)
                intent.putExtra("INTENT_EMAIL", email.text)
                startActivity(intent)
        } else {
            Toast.makeText(this, "¡Queremos conocerte mejor, brindanos tus datos!", Toast.LENGTH_LONG).show()
        }
    }

    fun msg(msg:String) { Toast.makeText(this, msg, Toast.LENGTH_LONG).show() }
}
